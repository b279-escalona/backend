// console.log("Hello World!")

// [SECTION] Objects

/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
- Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
    - Different data types may be stored in an object's property creating complex data structures
*/

let cellphone = {
	name: "Nokie 3310",
	manufacturedDate: 1999
};

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function

/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// this is an object
// "this" keyword allows to assign a new value to a new object.

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}


// This is a unique instance of the Laptop
//"new" keyword to create a new instance/copy 

let myLaptop = new Laptop("Lenovo",2008);
console.log('Result from creating object using object constructor:')
console.log(myLaptop);
// console.log(myLaptop.name);
// console.log(myLaptop.manufactureDate);

let laptop = new Laptop('Macbook Air', 2020);
console.log('Result from creating object using object constructor:')
console.log(laptop);

// We cant create an instance/copy without the usage of our 'new' keyword
let oldLaptop = Laptop("Portal R2E", 1980);
console.log('Result from creating object using object constructor:')
console.log(oldLaptop);


// Creating an empty object
let computer = {};
let myComputer = new Object()

// [SECTION] Accessing Array Object

/*
    - Accessing array elements can be also be done using square brackets
    - Accessing object properties using the square bracket notation and array indexes can cause confusion
    - By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
    - Object properties have names that makes it easier to associate pieces of information
*/

let array = [laptop, myLaptop];
// May be confused for accessing array index
console.log(array[0]["name"]);
// Differentiation between accessing an  array and object properties

// This tells us that array[0] is an object by using dot notation

console.log(array[1].name);

// [SECTION] Init/Adding/Deleting/Re-assigning Object Properties


let car = {};


// Init/Adding

car.name = "Honda Civic";
console.log('Result from adding property using dot notation:');
console.log(car);

// Using brackett notation
car["manufactureDate"] = 2019;
console.log(car["manufactureDate"]);
console.log(car.manufactureDate);
console.log(car);

// Deleting object properties

delete car["manufactureDate"];
console.log('Result from deleting properties');
console.log(car);

// Updating or Re-assigning property value
car.name = "Dodge Charger R/T";
console.log('Result from updating properties');
console.log(car);


// [SECTION] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

let person = {
	name: 'John',
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
}

console.log(person);
console.log('Result from object function/methods:');
person.talk();

// Adding methods to object
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}

person.walk()
console.log(person);

// Methods are useful for creating reusable functions that perform task related object

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ["joe@mail.com","joesmith@email.xyz"],
	introduce: function() {
		console.log("Hello my name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();

// [SECTION] Real World Application of Objects

/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log( "This Pokemon tackled targetPokemon");
        console.log( "targetPokemon's health is now reduced to _targetPokemonhealth_");
    },
    faint : function() {
     console.log("Pokemon fainted");
    }
}

console.log(myPokemon);

// Converting to object constructor
/*function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	}

	this.faint = function(){
		console.log(this.name + " fainted.");
	}


	
} */

// Create instance
let pikachu = new Pokemon("Pikachu", 12);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);

let trainer = {};

// Properties
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Balbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}
trainer.talk = function(){
	return "Pikachi! I choose you!";
}

// Checking of properties
console.log(trainer);

// Access name using dot notation
console.log('Result from dot notation');
console.log(trainer.name);


// Access name using bracket notation
console.log('Result from bracket notation');
console.log(trainer['pokemon']);

console.log(trainer.talk());

function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);

		target.health -= this.attack;

		// If the health of the target is less than 0
		if(target.health <= 0){
			target.faint();
		}else{
			return target.name + "'s health is now reduced to " + target.health;
		}

	

	}
	this.faint = function(){
		console.log( this.name +" fainted");
		// return this.name + " fainted";
	}

	
}

// Create instance

// Properties
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pickachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
	kanto: ["Brock", "Misty"],
	hoenn: ["May", "Max"]
}
trainer.talk = function(){
	return "Pickachu! I choose you!";
}

// checking of properties
console.log(trainer);

// Access name using dot notation
console.log("Result from dot notation");
console.log(trainer.name);

// Access pokemon using bracket notation
console.log("Result from bracket notation");
console.log(trainer["pokemon"]);

trainer.talk();

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
mewtwo.tackle(geodude);


mewtwo.tackle(pikachu);
console.log(geodude);
