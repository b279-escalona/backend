// Use "require" directive to load Node.js modules
// A "http module" lets Node.js transfer data using Hyper Text Transfer Protocol
// Clients (browser) and servers (node JS/express JS applications) communiate by exchanging individual messages



let http = require("http");

// Using this module's createServer() method, we can create a HTTP server that listens to requests on specific port.

// A port is a visual point where network connection start and end.
// Each port is associated with a specific process or service
// The server will be assigned to "port 4000"
http.createServer(function (request,response){
	// Use writeHead() method to:
	// Set a status code for the message - 200 means ok
	// Set the content-type of the response as a plain text 
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello Wolrd!");
}).listen(4000);

console.log("Server Running at localhost : 4000");
