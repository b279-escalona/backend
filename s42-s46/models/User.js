const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		requried : [true, "Firstname  is required!"]
	},
	lastName : {
		type : String,
		requried : [true, "Lastname  is required!"]
	},
	email : {
		type : String,
		requried : [true, "email  is required!"]
	},
	password : {
		type : String,
		requried : [true, "password  is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		requried : [true, "mobileNo  is required!"]
	},
	petsOwned : [
		{
			petId : {
				type : String,
				requried : [true, "Pet ID is required!"]
			},
			purchasedOn : {
				type : Date,
				default : new Date(),
			}
		} 
	]
})

module.exports = mongoose.model("User", userSchema);