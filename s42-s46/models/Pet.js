const mongoose = require("mongoose");

const petSchema = new mongoose.Schema({
	name : {
		type : String,
		requried : [true, "PET NAME  is required!"]
	},
	description : {
		type : String,
		requried : [true, "PET DESCRIPTION  is required!"]
	},
	price : {
		type : Number,
		requried : [true, "PET PRICE  is required!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression instantiates 
		default : new Date()
	},
	imgSource : {
		type : String,
		requried : [true, "Image source  is required!"]
	},
	buyer : [
		{
			userId : {
				type : String,
				requried : [true, "User ID is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			} 
		}

	]
})




module.exports = mongoose.model("Pet", petSchema);