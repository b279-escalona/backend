const User = require("../models/User");
const bcrypt = require("bcrypt");
const Pet = require("../models/Pet");
const auth = require("../auth");




// USER REGISTER
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {

		// User registration failed
		if (error) {

			return false;

		// User registration successful

		} else {

			return true;

		};

	});

};


// USER LOGIN
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false;

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }

			} else {

				return false;

			};

		};

	});

};


// USER PURCHASE (NON-ADMIN)
module.exports.purchase = async (data, isAdmin) => {

	if (isAdmin) {
		return "You cant't checkout because you are an ADMIN!"
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.petsOwned.push({petId : data.petId});

			return user.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		})

		let isPetUpdated = await Pet.findById(data.petId).then(pet => {
			pet.buyer.push({
				userId : data.userId,
			});

			return pet.save().then((pet, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		})

		if (isUserUpdated && isPetUpdated) {
			return true;
		} else {
			return false;
		}

	}

};


// GET USER DETAILS
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((result,error) => {
		if (error) {
			return false;
		}
		result.password = "";
		return result;
	});

};



// SET USER AS AN ADMIN

module.exports.userAdmin = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateAdminField = {
		isAdmin : true
	};

	if(isAdmin){
		return User.findByIdAndUpdate(reqParams.id, updateAdminField).then((user, error) => {

			// Course not archived
			if (error) {
				return false;
			// Course archived successfully
			} else {
				return true;
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};


// RETRIEVE ALL ORDERS (ADMIN)
/*module.exports.getAllOrders = (isAdmin) => {
	if (isAdmin) {
		return User.find({}).then(result => {
			return result;
		})
	} else {

		let message = Promise.resolve("You don't have the access rights to do this action.");

		return message.then((value) => {
			return value
		})
	}
};
*/
module.exports.getAllOrders = () => {
	return User.find({}).then(result => {
		result.firstName
		return result;
	})
};



// CHECK EMAIL IF EXISTING
module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		};
	});

};