const Pet = require("../models/Pet");


// CREATE PET (ADMIN)
module.exports.addPet = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newPet = new Pet({
			name: data.pet.name,
			description: data.pet.description,
			price: data.pet.price,
			imgSource : data.pet.imgSource
		})

		return newPet.save().then((pet, error) => {
			if(error){
				return error;
			}
			return pet;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};

// RETRIEVE ALL PETS
module.exports.getAllPets = () => {
	return Pet.find({}).then(result => {
		return result;
	})
};

// RETRIEVE ACTIVE PETS (ADMIN)
// module.exports.getAllActive = (isAdmin) => {
// 	if (isAdmin) {
// 		return Pet.find({isActive : true}).then(result => {
// 			return result;
// 		})
// 	} else {

// 		let message = Promise.resolve("You don't have the access rights to do this action.");

// 		return message.then((value) => {
// 			return value
// 		})
// 	}
// };


// RETRIEVE ACTIVE PETS (NON-ADMIN)
module.exports.getAllActive = () => {
		return Pet.find({isActive : true}).then(result => {
			return result;
		}) 	
};


// RETRIEVE SPECIFIC PET THRU PET ID
module.exports.getPet = (reqParams) => {
	return Pet.findById(reqParams.petId).then(result =>{
		return result;
	})
}



// UPDATE PET INFO (ADMIN)
module.exports.updatePet = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedPet = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		imgSource : reqBody.imgSource
		}

		return Pet.findByIdAndUpdate(reqParams.petId, updatedPet).then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}



// ARCHIVE PET (ADMIN)
module.exports.archivePet = (reqParams, isAdmin, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive
	};

	if(isAdmin){

		return Pet.findByIdAndUpdate(reqParams.petId, updateActiveField).then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		});
	} 

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
};


