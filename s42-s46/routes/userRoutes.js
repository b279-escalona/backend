const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


// USER REGISTRATION
router.post("/register" , (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});



// USER LOGIN
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// USER PURCHASE/CHECKOUT(NON-ADMIN)
router.post("/checkout" , auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization); 
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data = {
		userId : userData.id,
		petId : req.body.petId,
	}

	userController.purchase(data, isAdmin).then(resultFromController => res.send(resultFromController))
});

// GET USER DETAILS
router.get("/userDetails", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
});



// SET USER AS AN ADMIN (ADMIN)
router.put("/:id/setAsAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.userAdmin(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});



// RETRIEVE ALL ORDERS(ADMIN)
router.get("/orders", (req, res) => {
	// let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	
});


// CHECK EMAIL IF EXISTING
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});






// Allows us to export the router object that will access in our index.js file.
module.exports = router;