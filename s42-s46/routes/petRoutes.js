const express = require("express");
const router = express.Router();
const petController = require("../controllers/petController.js");
const auth = require("../auth.js");





// CREATE PET (ADMIN)
router.post("/products", auth.verify, (req, res) => {
	const data = {
		pet: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	petController.addPet(data).then(resultFromController => res.send(resultFromController));
});


// RETRIEVE ALL PETS
router.get("/products", (req, res) => {
	petController.getAllPets().then(resultFromController => res.send(resultFromController));
});


// // RETRIEVE ACTIVE PETS (ADMIN)
// router.get("/active", auth.verify, (req, res) => {
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	petController.getAllActive(isAdmin).then(resultFromController => res.send(resultFromController));
// });

// RETRIEVE ACTIVE PETS (NON-ADMIN)
router.get("/active", (req, res) => {
	petController.getAllActive().then(resultFromController => res.send(resultFromController));
});



// RETRIEVE SPECIFIC PET THRU PET ID

router.get("/:petId", (req, res) => {
	petController.getPet(req.params).then(resultFromController => res.send(resultFromController));

})




// UPDATE PET INFO (ADMIN)

router.put("/update", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	petController.updateInfo(isAdmin, req.body).then(resultFromController => res.send(resultFromController));
});


// UPDATE SPECIFIC PET
router.put("/:petId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	petController.updatePet(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

})




// ARCHIVE PET
router.put("/:petId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	petController.archivePet(req.params, isAdmin, req.body).then(resultFromController => res.send(resultFromController));

})




// Allows us to export the router object that will access in our index.js file.
module.exports = router;