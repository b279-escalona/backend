//  retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(result => console.log(result));


// create an array using the map method

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(result => {
	let titleMap = result.map((dataToMap) => dataToMap.title)
	console.log(titleMap);
});

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(result));


// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New To Do",
		body: "To Do List",
		userId: 1,
		completed : false
	})
})
.then(res => res.json())
.then(json => console.log(json)); 


// Update a to do list item by changing the data structure

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Updated list",
		description: "Updated list",
		status: true,
		dateCompleted : "May 3, 2023",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API

fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({ 
		dateCompleted: "May 3, 2023"
	})
})
.then(res => res.json())
.then(json => console.log(json));



// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "DELETE"
});