
// Insert single room

db.rooms.insertOne({
	name : "single",
	accomomodates : 2,
	price : 1000,
	descriptiont : "A simple room with all the basic necessities",
	rooms_available : 10,
	isAvailable : false
});


// Insert multiple rooms

db.rooms.insertMany([
	{
	name : "double",
	accomomodates : 3,
	price : 2000,
	descriptiont : "A room fit for a small family going on a vacation",
	rooms_available : 5,
	isAvailable : false
	},
	{
	name : "queen",
	accomomodates : 4,
	price : 4000,
	descriptiont : "A room with a queen sized bed perfect for a simple getaway",
	rooms_available : 5,
	isAvailable : false
	}
]);


// Find room : "double"

db.rooms.find({ name : "double"});

// Update queen : set the rooms to 0

db.rooms.updateOne(
	{ name : "queen"},
	{
		$set : {
			rooms_available : 0
		}
	}
);

// delete all rooms that have 0 rooms

db.rooms.deleteMany({
	rooms_available: 0
});
