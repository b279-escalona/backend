const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		requried : [true, "COURSE NAME  is required!"]
	},
	description : {
		type : String,
		requried : [true, "COURSE DESCRIPTION  is required!"]
	},
	price : {
		type : Number,
		requried : [true, "COURSE PRICE  is required!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression instantiates 
		default : new Date()
	},
	enrollees : [
		{
			userId : {
				type : String,
				requried : [true, "User ID is required"]
			},
			erolledOn : {
				type : Date,
				default : new Date()
			} 
		}

	]
})

module.exports = mongoose.model("Course", courseSchema);