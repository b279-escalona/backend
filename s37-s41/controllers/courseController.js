const Course = require("../models/Course");

// Create a new course
module.exports.addCourse = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return error;
			}
			return course;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

// retrieve all courses function
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// retrieve all courses function
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retreiving a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result =>{
		return result;
	})
}

// Update a course
module.exports.updateCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}else{
				return "Course Updated!";
			}
		})
	}else{
		return "You don't have the rights to do this action.";
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}


// Archive a course with isActive
module.exports.archiveCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	let updatedCourse = {
		isActive : reqBody.isActive,
		}

	if(isAdmin){
		

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}else{
				return "Course Updated!";
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

