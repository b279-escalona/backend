const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js");
const auth = require("../auth.js");



// route for echecking if email already exists
router.post("/checkEmail" , (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/register" , (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// route for user login
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Get user details using user Id

router.post("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
});


// Enroll user to a course
/*router.post("/enroll" , (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});
*/


// Enroll user to a course using token
// You can't enroll if you're NOT AN ADMIN

router.post("/enroll" , auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization); 
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data = {
		userId : userData.id,
		courseId : req.body.courseId
	}

	userController.enroll(data, isAdmin).then(resultFromController => res.send(resultFromController))
});











// Allows us to export the router object that will access in our index.js file.
module.exports = router;