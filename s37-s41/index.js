// SERVER CREATION AND DB CONNECTION
const express = require("express");
const mongoose = require("mongoose");

// Allows us to control the App's Cross Origin Resource Sharing (cors)
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course.js");


const app = express();



// Connecting to MongoDB
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.1erpgnu.mongodb.net/Course_Booking_System?retryWrites=true&w=majority",
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
);

// Optional : Validation of DB Connection
mongoose.connection.once("open" , () => console.log("Now connected to MongoDB Atlas")) ;




// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));


// Defines the "/users" string to be included for the user routes defined in the "user.js" rout file.
app.use("/users" , userRoutes);
app.use("/courses" , courseRoutes);




// PORT LISTING
if(require.main === module) {
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`));
}

module.exports = app;