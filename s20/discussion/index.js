// console.log("Hello World!")

// [SECTION] - Looping Statements
/*

1. While Loop
2. Do-While Loop
3. For Loop

*/

// While Loop
// Takes in an expression/condition
// Expression are any unit of codes that can be evaluated to a value
// If the condition is true, the statement or the code block will be executed.
// "Iteration" is the term given to the repetition of statements

/*
SYNTAX:

while(expression/condition) {
	statement/code block
}

*/

let count = 5;

// while the value of count is not equal to 0
while(count !== 0) {
	console.log("While: " + count);
	count-- ;

}

while(count !== 10) {
	console.log("While: " + count	);
	count++;
}

// Do While Loop
/*

do {
	statement
} while (expression/condition)


*/

let number = Number(prompt("Giev me a number"));

do {
	// The current value is printed out
	console.log("Do While: " + number);
	number++;
} while(number <= 30 );

// For Loop

/*
SYNTAX:

for(initialization; expression/condition; finalExpression) {
	statement/code block
}

*/

for (let count = 0; count <= 20; count++) {
	console.log(count)
}

// Accessing elements of a string
// Individual characters of a string may be accessed using index number
// index number starts from 0


let myString = "alex";
console.log(myString.length);

// Accessing element/character using index number
console.log(myString[2]);
console.log(myString[0]);
console.log(myString[3]);

// Will create a loop that will print out the individual letters of my syString

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

let myName = "ALEx";

for (let i = 0; i < myName.length; i++) {
	if(
		myName[i].toLowerCase() == "a" || myName[i].toLowerCase() == "e" || myName[i].toLowerCase() == "i" || myName[i].toLowerCase() == "o" || myName[i].toLowerCase() == "u"
		) {
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}

// [SECTION] Continue and Break Statement

for (let count = 0; count <= 69; count++) {
	if (count % 5 == 0 ) {
		continue;
		// this ignores all statements after continue keyword.
	}

	console.log("Continue and Break: " + count);

	if (count > 30) {
		break
		// Number values after 10 will no longer be printed
		// The process will be terminated
	}
} 

let name = "alexandro";

for (let i = 0; i < name.length; i++){
	

	if (name[i].toLowerCase() == "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	console.log(name[i]);

	if (name[i] == 'd' ){
		break;
	}
}