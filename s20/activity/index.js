// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for (x = 0; x < string.length; x++) {
	if (string[x] == "a" || string[x] == "e" || string[x] == "i" || string[x] == "o" || string[x] == "u") {
		continue;
	} else {
		filteredString += string[x]
	}
}
console.log(filteredString);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}