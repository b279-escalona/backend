// Count the total number of fruits on sale.

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "onSale", fruitsOnSale: {$sum: 1}}},
  {$project: {_id: 0}}
]);


// count the total number of fruits with stock more than 20.

db.fruits.aggregate([
  {$match: {stock: {$gte : 20}}},
  {$group: {_id: " ", enoughStock: {$sum: 1}}},
  {$project: {_id: 0}}
]);


// average price of fruits onSale per supplier
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", enoughStock: {$avg: "$price"}}},
]);

// get the highest price of a fruit per supplier
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
]);

// get the lowest price of a fruit per supplier
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$min: "$price"}}},
]);
